import sbt.*
import Keys.*

object Libs {

  val ZIO = "2.0.18"

  val dependencies = libraryDependencies ++= Seq(
    "dev.zio" %% "zio"        % ZIO,
    "dev.zio" %% "zio-lambda" % "1.0.4"
  )
}

import BuildHelper.*

name         := "hello-graalvm"
organization := "hello"

ThisBuild / scalaVersion      := "2.13.12"
ThisBuild / scalacOptions     := Settings.compilerOptions
ThisBuild / semanticdbEnabled := true
ThisBuild / semanticdbVersion := scalafixSemanticdb.revision
ThisBuild / scalafixDependencies ++= List(
  "com.github.liancheng" %% "organize-imports" % "0.6.0",
  "com.github.vovapolu"  %% "scaluzzi"         % "0.1.23"
)
ThisBuild / scalafixScalaBinaryVersion := "2.13"
ThisBuild / testFrameworks += new TestFramework("zio.test.sbt.ZTestFramework")

addCommandAlias("fmt", "; scalafmtSbt; scalafmtAll")
addCommandAlias("fix", "; scalafixAll; scalafmtSbt; scalafmtAll")
addCommandAlias("check", "; scalafmtSbtCheck; scalafmtCheckAll; scalafixAll --check")

lazy val root = (project in file("."))
  .enablePlugins(GraalVMNativeImagePlugin)
  .settings(
    welcomeMessage,
    publish / skip                 := true,
    name                           := "hello-graalvm",
    assembly / assemblyJarName     := "hello-graalvm.jar",
    GraalVMNativeImage / mainClass := Some("hello.HelloWorld"),
    graalVMNativeImageOptions := Seq(
      "--verbose",
      "--no-fallback",
      "--install-exit-handlers",
      "--enable-http",
      "--allow-incomplete-classpath",
      "--report-unsupported-elements-at-runtime",
      "-H:+StaticExecutableWithDynamicLibC",
      "-H:+RemoveSaturatedTypeFlows"
    ),
    Universal / mappings := Seq(
      file("bootstrap")                                 -> "bootstrap",
      file("target/graalvm-native-image/hello-graalvm") -> "hello-graalvm"
    ),
    Libs.dependencies
  )

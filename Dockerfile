FROM public.ecr.aws/amazonlinux/amazonlinux:2

# Install GraalVM and native-image tool
ENV GRAALVM_VERSION=21.1.0
ENV JAVA_HOME=/opt/graalvm-community-openjdk-17.0.9+9.1
ENV PATH=$PATH:${JAVA_HOME}/bin
ENV SBT_VERSION 1.9.7 

RUN yum -y update && yum -y install gcc zlib-devel tar gzip wget zlib-static
RUN curl -4 -L https://github.com/graalvm/graalvm-ce-builds/releases/download/jdk-17.0.9/graalvm-community-jdk-17.0.9_linux-x64_bin.tar.gz -o graalvm.tar.gz
RUN tar -xzf graalvm.tar.gz -C /opt/
RUN rm -f graalvm.tar.gz
RUN ${JAVA_HOME}/bin/gu install native-image

RUN rm -f /etc/yum.repos.d/bintray-rpm.repo && \
  curl -L https://www.scala-sbt.org/sbt-rpm.repo > sbt-rpm.repo && \
  mv sbt-rpm.repo /etc/yum.repos.d/ && \
  yum install sbt -y

RUN sbt -Dsbt.rootdir=true sbtVersion

WORKDIR /app
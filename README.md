### Build Docker Image with AWS Linux, GraalVM and SBT

`docker build -t graalvm-sbt .`

### Build GraalVM Native Binary

`docker run -v$(pwd):/app -w /app graalvm-sbt sbt clean compile graalvm-native-image:packageBin Universal/packageBin`

This builds a zip file in `target/universal` containing the native binary executable and a `bootstrap` file for AWS Lambda.

### AWS Lambda Console - Create Function

Specify runtime as _Provide your own bootstrap on Amazon Linux 2_

### AWS Lambda Console - Test

Upload zip file and test by creating a test event with a string value.
package hello

import zio.*
import zio.lambda.*
import scala.annotation.nowarn

object HelloWorld extends ZIOAppDefault {

  @nowarn
  def app(request: String, context: Context): Task[String] = {
    val output = s"Hello World from $request"
    Console.printLine(output).as(s"Handler ran and printed the message - $output")
  }

  override val run: Task[Unit] =
    ZLambdaRunner.serve(app)
}
